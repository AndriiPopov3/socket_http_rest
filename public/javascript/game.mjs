import { createElement, removeClass, addClass } from "./helper.mjs";
import { showModal } from "./modal.mjs";

const username = sessionStorage.getItem("username");

const socket = io("http://localhost:3002/game", { query: { username } });

const addRoomButton = document.getElementById("add-room-btn");

const roomList = document.getElementById("room-list");

// flag to stop timer once the game is over
let isGameOn = false;

if (!username) {
  window.location.replace("/login");
}

if (username) {
  socket.emit("USERNAME_UNIQUE", username);
}

const usernameExists = username => {
  alert(`Error: username ${username} exists`);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
};

const createRoomButton = (room, roomNum) => {
    const roomBlock = createElement({
      tagName: "div",
      className: "room",
      attributes: { id: room }
    });
  
    const roomTitle = createElement({
      tagName: "h3"
    });
    roomTitle.innerText = room;
  
    const userCount = createElement({
      tagName: "h5"
    });
    userCount.innerText = `${roomNum} user connected`;
  
    const joinRoomButton = createElement({
      tagName: "button",
      className: "join-btn"
    });
    joinRoomButton.innerText = "Join Room";
    joinRoomButton.addEventListener("click", () => joinRoom(room));
  
    roomBlock.append(roomTitle, userCount, joinRoomButton);
  
    return roomBlock;
};

const updateRooms = rooms => {
  const allRooms = [];
  if (rooms) {
    for (let i=0; i < rooms[0]?.length; i++) {
      allRooms.push(createRoomButton(rooms[0][i], rooms[1][i]));
    }
    roomList.innerHTML = "";
    roomList.append(...allRooms);
  }
};

const addRoom = () => {
  const room_name = prompt("enter room name:", '');
  if (room_name) {
    socket.emit("CREATE_ROOM", room_name);
  }
};

const joinRoom = room_name => {
  socket.emit("JOIN_ROOM", room_name);
}

const leaveRoom = room_name => {
  socket.emit("LEAVE_ROOM", room_name);
}

const leaveRoomDone = rooms => {
  const roomsPage = document.getElementById("rooms-page");
  removeClass(roomsPage, "display-none");
  const gamePage = document.getElementById("game-page");
  addClass(gamePage, "display-none");
  updateRooms(rooms);
}

const onExistingRoomName = room => {
  alert(`Error: Room name '${room}' already exists`);
}

const onMaxUsers = room => {
  alert(`Room '${room}' already has maximum number of players`);
}

const userReady = (room, user) => {
  socket.emit("USER_READY", {room, user});
}

const userNotReady = (room, user) => {
  socket.emit("USER_NOT_READY", {room, user});
}

const userReadyDone = obj => {
  document.getElementById(obj.user).setAttribute("class", "ready-status-green");
  const rightContainer = document.getElementById("right-container");
  rightContainer.innerHTML = "";
  const readyButton = createElement({
    tagName: "button",
    attributes: {
      id: "ready-btn"
    }
  });
  readyButton.innerText = "NOT READY";
  readyButton.addEventListener("click", () => userNotReady(obj.room, obj.user));
  rightContainer.append(readyButton);
}

const userNotReadyDone = obj => {
  console.log(obj);
  document.getElementById(obj.user).setAttribute("class", "ready-status-red");
  const rightContainer = document.getElementById("right-container");
  rightContainer.innerHTML = "";
  const readyButton = createElement({
    tagName: "button",
    attributes: {
      id: "ready-btn"
    }
  });
  readyButton.innerText = "READY";
  readyButton.addEventListener("click", () => userReady(obj.room, obj.user));
  rightContainer.append(readyButton);
}

const userReadyDoneRoom = user => {
  document.getElementById(user.user).setAttribute("class", "ready-status-green");
}

const userNotReadyDoneRoom = user => {
  document.getElementById(user.user).setAttribute("class", "ready-status-red");
}

const areUsersReady = room_data => {
  const greenIcons = document.getElementsByClassName("ready-status-green");
  console.log(room_data.roomUsersNum);
  if(greenIcons.length === room_data.roomUsersNum) {
    beginCountdown(room_data.room, room_data.user);
  }
}

const beginCountdown = (room, user) => {
  socket.emit("START_COUNTDOWN", {room});
}

const beginCountdownDone = room_data => {
  socket.emit("START_GAME", room_data);
}

const beginGame = room_data => {
  const readyButton = document.getElementById("ready-btn");
  const rightContainer = document.getElementById("right-container");
  const leaveRoomButton = document.getElementById("quit-room-btn");
  addClass(readyButton, "display-none");
  addClass(leaveRoomButton, "display-none");
  const countdown = createElement({
    tagName: "div",
    attributes: {
      id: "timer"
    }
  });
  rightContainer.append(countdown);
  let gameText = "";
  fetch(`http://localhost:3002/game/texts/${room_data.text}`)
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    console.log(data);
    gameText = data;
  });
  let countdownTime = new Date().getTime();
  countdownTime = countdownTime + (room_data.time * 1000);
  const x = setInterval(function() {
    let now = new Date().getTime();
    let distance = countdownTime - now;
    document.getElementById("timer").innerHTML = Math.floor(distance / 1000);
    if (distance < 1) {
      clearInterval(x);
      const rightContainer = document.getElementById("right-container");
      addClass(document.getElementById("timer"), "display-none");

      const textContainer = createElement({
        tagName: "div",
        attributes: {
          id: "text-container"
        }
      });
      for (let i = 0; i < gameText.text.length; i++) {
        let span = createElement({
          tagName: "span",
          className: "span"
        });
        span.innerText = gameText.text.slice(i, i+1);
        textContainer.append(span);
      }
      rightContainer.append(textContainer);
      startGame(room_data.gameTime, room_data.user, room_data.room);
    }
  }, 1000);
}

const startGame = (time, user, room) => {
  isGameOn = true;
  document.addEventListener("keydown", typing, false);
  const spans = document.querySelectorAll(".span");
  spans[0].classList.add("nextChar");
  const rightContainer = document.getElementById("right-container");
  function typing(event) {
    let name = event.key;
    for (let i = 0; i < spans.length; i++) {
      if (spans[i].innerHTML === name) {
        if (spans[i].classList.contains("bg")) { // if it already has class with the background color then check the next one
          continue;
        } else if (spans[i].classList.contains("bg") === false && spans[i-1] === undefined || spans[i-1].classList.contains("bg") !== false ) { 
          spans[i].classList.remove("nextChar");
          spans[i].classList.add("bg");
          if(i+1 !== spans.length) {
            spans[i+1].classList.add("nextChar");
          }
          socket.emit("UPDATE_PROGRESS_BARS", {room, user, current: i+1, full: spans.length});
          break;
        }
      }
    }
  };
  const gameCountdown = createElement({
    tagName: "div",
    attributes: {
      id: "game-timer"
    }
  });
  rightContainer.append(gameCountdown);
  let countdownTime = new Date().getTime();
  countdownTime = countdownTime + (time * 1000);
  const x = setInterval(function() {
    let now = new Date().getTime();
    let distance = countdownTime - now;
    document.getElementById("game-timer").innerHTML = Math.floor(distance / 1000) + " seconds left";
    if (distance < 1) {
      clearInterval(x);
      document.removeEventListener("keydown", typing, false);
      socket.emit("GAME_TIMEOUT", {
        room,
        user
      });
    }
    if (isGameOn === false) {
      clearInterval(x);
      document.removeEventListener("keydown", typing, false);
    }
  }, 1000);
}

const updateProgressBars = user_progress_data => {
  const userProgress = document.getElementsByClassName(`user-progress ${user_progress_data.user}`);
  userProgress[0].style.width = (user_progress_data.current === user_progress_data.full) ? "100%" : ((user_progress_data.current / user_progress_data.full) * 100) + "%" ;
  if (userProgress[0].style.width === "100%") {
    userProgress[0].classList.add("finished");
    const finisherNum = document.getElementsByClassName("finished").length;
    socket.emit("ADD_FINISHER", { user: user_progress_data.user, rank: finisherNum, room: user_progress_data.room});
  }
}

const finishGameDone = room_winnerList => {
  isGameOn = false;
  const winners = Object.values(room_winnerList.winnerList);
  const ranks = Object.keys(room_winnerList.winnerList);
  let new_ranks = [];
  if(ranks[0] !== 1) {
    const diff = ranks[0] - 1;
    ranks.map(rank => new_ranks.push(rank - diff));
  }
  const winnerContainer = createElement({
    tagName: "div",
    attributes: {
      id: "winner-container"
    }
  });
  for (let i = 0; i < winners.length; i++) {
    const winnerBlock = createElement({
      tagName: "div",
      className: "winner-block"
    });
    if(new_ranks.length !== 0) {
      winnerBlock.innerText = `${new_ranks[i]}. ${winners[i]}`;
    } else {
      winnerBlock.innerText = `${ranks[i]}. ${winners[i]}`;
    }
    winnerContainer.append(winnerBlock);
    const username = document.getElementsByClassName(`user-name ${winners[i]}`)[0];
    username.id = username.id + `place - ${ranks[i]}`;
  }
  addClass(document.getElementById("game-timer"), "display-none");
  addClass(document.getElementById("text-container"), "display-none");
  showModal({title: `Match ranking`, bodyElement: winnerContainer, onClose: () => {
    const readyButton = document.getElementById("ready-btn");
    const leaveRoomButton = document.getElementById("quit-room-btn");
    removeClass(readyButton, "display-none");
    removeClass(leaveRoomButton, "display-none");
    socket.emit("UPDATE_INTERFACE_AFTER_GAME_OVER", {
      room: room_winnerList.room,
      user: room_winnerList.username
    });
    socket.emit("USER_NOT_READY", {
      room: room_winnerList.room,
      user: room_winnerList.username
    });
  }});
}

const timeoutGameDone = room_userList => {
  isGameOn = false;

  const winners = Object.values(room_userList.winnerList);
  const ranks = Object.keys(room_userList.winnerList);
  let new_ranks = [];
  if(ranks[0] !== 1) {
    const diff = ranks[0] - 1;
    ranks.map(rank => new_ranks.push(rank - diff));
  }
  const winnerContainer = createElement({
    tagName: "div",
    attributes: {
      id: "winner-container"
    }
  });
  let winCounter = 0;
  for (let i = 0; i < winners.length; i++) {
    winCounter = winCounter + 1;
    room_userList.userList.splice(room_userList.userList.indexOf(winners[i]), 1);
    const winnerBlock = createElement({
      tagName: "div",
      className: "winner-block"
    });
    if(new_ranks.length !== 0) {
      winnerBlock.innerText = `${new_ranks[i]}. ${winners[i]}`;
    } else {
      winnerBlock.innerText = `${ranks[i]}. ${winners[i]}`;
    }
    winnerContainer.append(winnerBlock);
    const username = document.getElementsByClassName(`user-name ${winners[i]}`)[0];
    username.id = username.id + `place - ${ranks[i]}`;
  }

  const runnerUps = [];
  for (let i = 0; i < room_userList.userList.length; i++) {
    const width = document.getElementsByClassName(`user-progress ${room_userList.userList[i]}`)[0].style.width;
    runnerUps.push({
      user: room_userList.userList[i],
      score: +width.slice(0, width.length-1)
    });
  }
  runnerUps.sort((a,b) => (a.score > b.score) ? -1 : ((b.score > a.score) ? 1 : 0));
  for (let i = 0; i < runnerUps.length; i++) {
    const winnerBlock = createElement({
      tagName: "div",
      className: "winner-block"
    });
    winnerBlock.innerText = `${winCounter + 1}. ${runnerUps[i].user}`;
    winCounter = winCounter + 1;
    winnerContainer.append(winnerBlock);
    const username = document.getElementsByClassName(`user-name ${runnerUps[i].user}`)[0];
    username.id = username.id + `place - ${i + 1}`;
  }
  addClass(document.getElementById("game-timer"), "display-none");
  addClass(document.getElementById("text-container"), "display-none");
  showModal({title: `Match ranking`, bodyElement: winnerContainer, onClose: () => {
    const readyButton = document.getElementById("ready-btn");
    const leaveRoomButton = document.getElementById("quit-room-btn");
    removeClass(readyButton, "display-none");
    removeClass(leaveRoomButton, "display-none");
    socket.emit("UPDATE_INTERFACE_AFTER_GAME_OVER", {
      room: room_userList.room,
      user: room_userList.username
    });
    socket.emit("USER_NOT_READY", {
      room: room_userList.room,
      user: room_userList.username
    });
  }});
}

const createRoomInterface = room_user => {
  const roomsPage = document.getElementById("rooms-page");
  addClass(roomsPage, "display-none");
  const gamePage = document.getElementById("game-page");
  removeClass(gamePage, "display-none");
  gamePage.innerHTML = "";
  // game page creation START
  const mainContainer = createElement({
    tagName: "div",
    attributes: {
      id: "main-container"
    }
  });
  const leftContainer = createElement({
    tagName: "div",
    attributes: {
      id: "left-container"
    }
  });
  const roomTitle = createElement({
    tagName: "h1"
  });
  roomTitle.innerText = room_user.room_name;

  const leaveRoomButton = createElement({
    tagName: "button",
    attributes: {
      id: "quit-room-btn"
    }
  });
  leaveRoomButton.innerText = "Leave Room";
  leaveRoomButton.addEventListener("click", () => leaveRoom(room_user.room_name));

  const userContainer = createElement({
    tagName: "div",
    attributes: {
      id: "user-container"
    }
  });
  
  const roomUsers = room_user.userList.map(user => createUserBlock(user))

  const rightContainer = createElement({
    tagName: "div",
    attributes: {
      id: "right-container"
    }
  });

  const readyButton = createElement({
    tagName: "button",
    attributes: {
      id: "ready-btn"
    }
  });
  readyButton.innerText = "READY";
  readyButton.addEventListener("click", () => userReady(room_user.room_name, room_user.username));
  userContainer.append(...roomUsers);
  leftContainer.append(roomTitle, leaveRoomButton, userContainer);
  rightContainer.append(readyButton);
  mainContainer.append(leftContainer, rightContainer);
  gamePage.append(mainContainer);
  // game page creation END
}

const createUserBlock = user => {
  const userBlock = createElement({
    tagName: "div",
    className: "user-block"
  });
  const userInfo = createElement({
    tagName: "div",
    className: "user-block-info"
  });
  const userStatus = createElement({
    tagName: "div",
    className: "ready-status-red",
    attributes: {
      id: `${user}`
    }
  });
  const user_name = createElement({
    tagName: "h4",
    className: `user-name ${user}`
  })
  user_name.innerText = user;
  const progressBarIndicator = createElement({
    tagName: "div",
    className: `user-progress-indicator`
  });
  const progressBar = createElement({
    tagName: "div",
    className: `user-progress ${user}`
  });
  progressBarIndicator.append(progressBar);
  userInfo.append(userStatus, user_name);
  userBlock.append(userInfo, progressBarIndicator);
  return userBlock;
}

const updateRoomInterface = room_user => {
  const userContainer = document.getElementById("user-container");
  userContainer.innerHTML = "";
  const roomUsers = room_user.userList.map(user => createUserBlock(user));
  userContainer.append(...roomUsers);
}

addRoomButton.addEventListener("click", addRoom);

socket.on("USERNAME_EXISTS", usernameExists);
socket.on("UPDATE_ROOMS", updateRooms);
socket.on("LEAVE_ROOM_DONE", leaveRoomDone);
socket.on("CREATE_ROOM_INTERFACE", createRoomInterface);
socket.on("UPDATE_ROOM_INTERFACE", updateRoomInterface);
socket.on("ROOM_NAME_EXISTS", onExistingRoomName);
socket.on("ROOM_HAS_MAX_USERS", onMaxUsers);
socket.on("USER_READY_DONE", userReadyDone);
socket.on("USER_READY_DONE_ROOM", userReadyDoneRoom);
socket.on("USER_NOT_READY_DONE", userNotReadyDone);
socket.on("USER_NOT_READY_DONE_ROOM", userNotReadyDoneRoom);
socket.on("ARE_USERS_READY", areUsersReady);
socket.on("START_COUNTDOWN_DONE", beginCountdownDone);
socket.on("START_GAME_DONE", beginGame);
socket.on("UPDATE_PROGRESS_BARS_DONE", updateProgressBars);
socket.on("FINISH_GAME_DONE", finishGameDone);
socket.on("TIMEOUT_GAME_DONE", timeoutGameDone);