import * as config from "./config";
import { users, users_ready } from "./users";
import {texts} from "../data";

const rooms = [];
const rooms_nums = {};


export default io => {
    io.on("connection", socket => {
        socket.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
        let currentRoomId;
        let username = users.get(socket.id.slice(6));
        let winnerList = {};

        socket.on("USERNAME_UNIQUE", user => {
            let counter = 0;
            for (let [k, v] of users) {
                if (v === user) {
                    counter = counter + 1;
                    if(counter === 2) {
                        users.delete(user);
                        socket.emit("USERNAME_EXISTS", user);
                        break;
                    } 
                }
            } 
        });

        socket.on("CREATE_ROOM", (room_name) => {
            if (rooms.indexOf(room_name) === -1) { // room name validation
                socket.join(room_name, () => {
                    rooms.push(room_name);
                    let userList = [];
                    io.in(room_name).clients((error, clients) => {
                        if (error) throw error; 
                        rooms_nums[room_name] = clients.length;
                        currentRoomId = room_name;
                        socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
                        clients.map(client => userList.push(users.get(client.slice(6))));
                        socket.emit("CREATE_ROOM_INTERFACE", {room_name, userList, username});
                    });
                });
            } else {
                socket.emit("ROOM_NAME_EXISTS", room_name);
            }
        });

        socket.on("JOIN_ROOM", (room_name) => {
            if (rooms_nums[room_name] === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
                socket.emit("ROOM_HAS_MAX_USERS", room_name);
            } else {
                socket.join(room_name, () => {
                    let userList = [];
                    rooms_nums[room_name] = rooms_nums[room_name] + 1;
                    io.in(room_name).clients((error, clients) => {
                        if (error) throw error; 
                        currentRoomId = room_name;
                        clients.map(client => userList.push(users.get(client.slice(6))));
                        socket.emit("CREATE_ROOM_INTERFACE", {room_name, userList, username});
                        socket.to(room_name).emit("UPDATE_ROOM_INTERFACE", {room_name, userList, username});
                        
                    });
                    socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
                });
            }
        });

        socket.on("LEAVE_ROOM", (room_name) => {
            socket.leave(room_name, () => {
                rooms_nums[room_name] = rooms_nums[room_name] - 1;
                if (rooms_nums[room_name] === 0) {
                    rooms.splice(rooms.indexOf(room_name), 1);
                    delete rooms_nums[room_name];
                }
                currentRoomId = "";
                let userList = [];
                io.in(room_name).clients((error, clients) => {
                    if (error) throw error; 
                    clients.map(client => userList.push(users.get(client.slice(6))));
                    socket.to(room_name).emit("UPDATE_ROOM_INTERFACE", {room_name, userList, username});
                });
                socket.emit("LEAVE_ROOM_DONE", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
                socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
            });
        });

        socket.on("USER_READY", room_user => {
            const room = room_user.room;
            const user = room_user.user;
            let roomUsersNum;
            io.in(room).clients((error, clients) => {
                if (error) throw error; 
                roomUsersNum = clients.length;
                socket.emit("USER_READY_DONE", {room, user});
                socket.to(room_user.room).emit("USER_READY_DONE_ROOM", {room, user});
                socket.emit("ARE_USERS_READY", {room, user, roomUsersNum});
            });
        });

        socket.on("USER_NOT_READY", room_user => {
            const room = room_user.room;
            const user = room_user.user;;
            socket.emit("USER_NOT_READY_DONE", {room, user});
            socket.to(room_user.room).emit("USER_NOT_READY_DONE_ROOM", {room, user});
        });

        socket.on("START_COUNTDOWN", room => {
            rooms.splice(rooms.indexOf(room.room), 1);
            delete rooms_nums[room.room];
            socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
            const textId = Math.floor(Math.random() * texts.length);
            socket.emit("START_COUNTDOWN_DONE", {
                time: config.SECONDS_TIMER_BEFORE_START_GAME, 
                gameTime: config.SECONDS_FOR_GAME, 
                text: textId, 
                room: room.room
            });
            socket.to(room.room).emit("START_COUNTDOWN_DONE", {
                time: config.SECONDS_TIMER_BEFORE_START_GAME, 
                gameTime: config.SECONDS_FOR_GAME, 
                text: textId, 
                room: room.room
            });
        });

        socket.on("START_GAME", room_data => {
            socket.emit("START_GAME_DONE", {
                time: room_data.time, 
                gameTime: room_data.gameTime, 
                text: room_data.text, 
                room: room_data.room,
                user: username
            });
        });

        socket.on("UPDATE_PROGRESS_BARS", room_user => {
            socket.emit("UPDATE_PROGRESS_BARS_DONE", {
                user: room_user.user,
                current: room_user.current,
                full: room_user.full,
                room: room_user.room
            });
            socket.to(room_user.room).emit("UPDATE_PROGRESS_BARS_DONE", {
                user: room_user.user,
                current: room_user.current,
                full: room_user.full,
                room: room_user.room
            });
        });

        socket.on("ADD_FINISHER", user_win => {
            winnerList[user_win.rank] = user_win.user;
            let userList = [];
            io.in(user_win.room).clients((error, clients) => {
                if (error) throw error; 
                if (clients.length === Object.keys(winnerList).length) {
                    clients.map(client => userList.push(users.get(client.slice(6))));
                    for (let i = 0; i < Object.values(winnerList).length; i++) {
                        if (!userList.includes(Object.values(winnerList)[i])) {
                            const winnerRank = Object.keys(winnerList).find(key => winnerList[key] === Object.values(winnerList)[i]);
                            delete winnerList[winnerRank];
                        }
                    }
                    socket.emit("FINISH_GAME_DONE", {room: user_win.room, winnerList, username});
                }
            });
        });

        socket.on("FINISH_GAME", room => {
            let userList = [];
            io.in(room.room).clients((error, clients) => {
                if (error) throw error; 
                clients.map(client => userList.push(users.get(client.slice(6))));
                for (let i = 0; i < Object.values(winnerList).length; i++) {
                    if (!userList.includes(Object.values(winnerList)[i])) {
                        const winnerRank = Object.keys(winnerList).find(key => winnerList[key] === Object.values(winnerList)[i]);
                        delete winnerList[winnerRank];
                    }
                }
                socket.emit("FINISH_GAME_DONE", {room: room.room, winnerList, username});
            });
        });

        socket.on("UPDATE_INTERFACE_AFTER_GAME_OVER", room_user => {
            winnerList = {};
            let userList = [];
            io.in(room_user.room).clients((error, clients) => {
                if (error) throw error; 
                rooms_nums[room_user.room] = clients.length;
                clients.map(client => userList.push(users.get(client.slice(6))));
                socket.emit("UPDATE_ROOM_INTERFACE", {
                    room: room_user.room,
                    userList,
                    user: room_user.user
                });
                socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
            });
            rooms.push(room_user.room);
        });

        socket.on("GAME_TIMEOUT", user_room => {
            let userList = [];
            io.in(user_room.room).clients((error, clients) => {
                if (error) throw error; 
                clients.map(client => userList.push(users.get(client.slice(6))));
                socket.emit("TIMEOUT_GAME_DONE", {room: user_room.room, winnerList, userList, username});
            });
        });

        socket.on("disconnect", () => {
            console.log(`${socket.id} disconnected`);
            users.delete(socket.id.slice(6)); // removes user from map
            if(currentRoomId) {
                if(rooms_nums[currentRoomId]) {
                    rooms_nums[currentRoomId] = rooms_nums[currentRoomId] - 1;
                }
                if (rooms_nums[currentRoomId] === 0) {
                    rooms.splice(rooms.indexOf(currentRoomId), 1);
                    delete rooms_nums[currentRoomId];
                }
                winnerList = {};
            }
            socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
        });
    });
};