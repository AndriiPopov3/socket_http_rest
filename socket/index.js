import * as config from "./config";
import game from "./game";
import { users, users_ready } from "./users";

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
      users.set(socket.id, username);
  });
  game(io.of("/game"));
};
